<?php

namespace App\dto;

/**
 * Лог одной совершаемой операции в калькуляторе
 */
class OptionLog
{
    public function __construct(
        public ?float $currentResult,
        public string $action,
        public ?float $newResult
    ){}
}