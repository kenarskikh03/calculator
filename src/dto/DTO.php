<?php

namespace App\dto;

/**
 * DTO для калькулятора
 */
class DTO
{
    public function __construct(
        public int $firstNum,
        public int $secondNum,
        public ?float $result = 0,
        public float $countIterations = 0,
        public array $successLog = [],
        public array $failLogs = []
    ){}

    /**
     * @return void
     */
    public function showWeb(): void
    {
        echo 'Найдена удачная комбинация:<br/>';
        echo "Число 1 - $this->firstNum<br/>";
        echo "Число 2 - $this->secondNum<br/>";
        echo 'Последовательность действий:<br/>';
        foreach ($this->successLog as $operation) {
            echo $operation['operationTitle'] . ' ';
        }
        echo '<br/>';

        echo "Выполнено итераций $this->countIterations<br/>";

        echo 'Лог выполнения:<br/>';

        foreach ($this->successLog as $operation) {
            echo "Текущий результат: {$operation['currentResult']}<br/>";
            echo "Выполнено действие: {$operation['operationTitle']}. Результат {$operation['newResult']} <br/>";
        }

        echo 'Неудачные комбинации:<br/>';

        foreach ($this->failLogs as $log) {
            foreach ($log as $operation) {
                echo $operation['operationTitle'] . ' ';
            }
            echo '<br/>';
        }
    }

    /**
     * @return void
     */
    public function showConsole(): void
    {
        $colorRed = "\033[31m";
        $colorGreen = "\033[32m";
        $colorYellow = "\033[33m";
        $colorBlue = "\033[34m";
        $colorReset = "\033[0m";

        echo $colorGreen . 'Найдена удачная комбинация:' . PHP_EOL . $colorReset;
        echo "Число 1 - $this->firstNum" . PHP_EOL;
        echo "Число 2 - $this->secondNum" . PHP_EOL;
        echo $colorGreen . 'Последовательность действий:' . PHP_EOL . $colorYellow;

        foreach ($this->successLog as $operation) {
            echo $operation['operationTitle'] . ' ';
        }

        echo PHP_EOL . $colorReset;

        echo $colorBlue . "Выполнено итераций $this->countIterations" . PHP_EOL . $colorReset;

        echo 'Лог выполнений:' . PHP_EOL;

        foreach ($this->successLog as $operation) {
            echo "Текущий результат: {$operation['currentResult']}" . PHP_EOL;
            echo "Выполнено действие: {$operation['operationTitle']}. Результат {$operation['newResult']}" . PHP_EOL;
        }

        echo $colorRed . 'Неудачные комбинации:' . PHP_EOL;

        foreach ($this->failLogs as $log) {
            foreach ($log as $operation) {
                echo $operation['operationTitle']. ' ';
            }
            echo PHP_EOL;
        }

        echo $colorReset;
    }
}
