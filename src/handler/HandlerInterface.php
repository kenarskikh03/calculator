<?php

namespace App\handler;

use App\dto\DTO;

/**
 * Интерфейс хендлера калькулятора
 */
interface HandlerInterface
{
    /**
     * @param DTO $context
     * @return void
     */
public function handle(DTO $context) : void;
}
