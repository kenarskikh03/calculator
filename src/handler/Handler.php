<?php

namespace App\handler;

use App\dto\DTO;

class Handler implements HandlerInterface
{
    public function __construct(private array $operationStrategies)
    {}

    /**
     * @param DTO $context контекст для вычислений
     * @return void
     */
    public function handle(DTO $context): void
    {
        while (empty($context->successLog) && $context->countIterations < 30) {
            $context->countIterations++;
            $context->result = 0;
            shuffle($this->operationStrategies);

            $this->checkOperations($context);
        }
    }

    /**
     * @param DTO $context контекст для вычислений
     * @return void
     */
    private function checkOperations(DTO $context): void
    {
        $combinationLog = [];
        $isSuccess = true;

        foreach ($this->operationStrategies as $operation) {
            $currentResult = $context->result;
            $operation->calculate($context);

            $combinationLog[] = [
                'currentResult' => $currentResult,
                'operationTitle' =>$operation->title,
                'newResult' => $context->result
            ];

            if ($context->result === null) {
                $isSuccess = false;
            }
        }

        if ($isSuccess) {
            $context->successLog = $combinationLog;
        }

        if ($isSuccess === false) {
            $context->failLogs[] = $combinationLog;
        }
    }
}
