<?php

namespace App\strategy;

use App\dto\DTO;

/**
 * Операция сложения
 */
class AdditionStrategy implements CalculatorStrategy
{
    public string $title = 'сложение';

    /**
     * @param DTO $dto контекст для вычисления
     * @return void
     */
    public function calculate(DTO $dto): void
    {
        $dto->result = $dto->result >= 0 ? $dto->firstNum + $dto->secondNum : null;
    }
}
