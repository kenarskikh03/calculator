<?php

namespace App\strategy;

use App\dto\DTO;

/**
 * Операция вычитания
 */
class SubtractionStrategy implements CalculatorStrategy
{
    public string $title = 'вычитание';

    /**
     * @param DTO $dto контекст для вычисления
     * @return void
     */
    public function calculate(DTO $dto): void
    {
        $dto->result = $dto->result < 1000 ? $dto->firstNum - $dto->secondNum : null;
    }
}
