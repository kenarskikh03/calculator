<?php

namespace App\strategy;

use app\dto\DTO;

/**
 * Интерфейс для стратегий операций в калькуляторе
 */
interface CalculatorStrategy
{
    /**
     * @param DTO $dto контекст для вычисления
     * @return void
     */
    public function calculate(DTO $dto): void;
}
