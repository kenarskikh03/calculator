<?php

namespace App\strategy;

use App\dto\DTO;

/**
 * Операция умножения
 */
class MultiplicationStrategy implements CalculatorStrategy
{
    public string $title = 'умножение';

    /**
     * @param DTO $dto контекст для вычисления
     * @return void
     */
    public function calculate(DTO $dto): void
    {
        $dto->result = $dto->result > 10 ?  $dto->firstNum * $dto->secondNum : null;
    }
}
