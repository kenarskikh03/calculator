<?php

namespace App\strategy;

use App\dto\DTO;

/**
 * Операция деления
 */
class DivisionStrategy implements CalculatorStrategy
{
    public string $title = 'деление';

    /**
     * @param DTO $dto контекст для вычисления
     * @return void
     */
    public function calculate(DTO $dto): void
    {
        $dto->result = $dto->result > 1000 ? $dto->firstNum / $dto->secondNum : null;
    }
}
