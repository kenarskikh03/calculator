<?php

require '../vendor/autoload.php';

use App\dto\DTO;
use App\handler\Handler;
use App\strategy\AdditionStrategy;
use App\strategy\SubtractionStrategy;
use App\strategy\MultiplicationStrategy;
use App\strategy\DivisionStrategy;

$dto = new DTO(85, 17);
$handler = new Handler([new AdditionStrategy(), new SubtractionStrategy(), new MultiplicationStrategy(), new DivisionStrategy()]);
$handler->handle($dto);
$dto->showWeb();
